<!--NAVIGASI MENU UTAMA-->

<div class="leftmenu">
  <ul class="nav nav-tabs nav-stacked">
    <li class="active"><a href="dashboard.php"><span class="icon-align-justify"></span> DASHBOARD</a></li>
    
    <!--MENU administrator-->
    <?php
	if($_SESSION['login_hash']=="administrator")
	{
	?>
    <li class="dropdown"><a href="#"><span class="icon-th-list"></span> DATA MASTER</a>
      <ul>
        <li><a href="?cat=administrator&page=barang">BARANG JADI</a></li>
        <li><a href="?cat=administrator&page=bahan">BAHAN</a></li>    
      </ul>
    </li>
    <li class="dropdown"><a href="#"><span class="icon-pencil"></span> STOK BAHAN</a>
      <ul>
        <li><a href="?cat=administrator&page=entry_bahan">ENTRY BAHAN</a></li>
        <li><a href="?cat=administrator&page=bahandigunakan">BAHAN DIGUNAKAN</a></li>       
      </ul>
    </li>	
    <li class="dropdown"><a href="#"><span class="icon-pencil"></span>STOK BARANG</a>
      <ul>
        <li><a href="?cat=administrator&page=entry">ENTRY BARANG JADI</a></li>      
      </ul>
    </li>
    <li class="dropdown"><a href="#"><span class="icon-pencil"></span>LAPORAN</a>
      <ul>
        <li><a href="?cat=administrator&page=monthreporting">LAPORAN ENTRY BARANG JADI</a></li>       
        <li><a href="?cat=administrator&page=laporanpemesanan">LAPORAN PEMESANAN</a></li>       
        <li><a href="?cat=administrator&page=laporanbahan">LAPORAN BAHAN MASUK DAN BAHAN DI GUNAKAN</a></li>       
      </ul>
    </li>
    <li class="dropdown"><a href="#"><span class="icon-pencil"></span> DATA PESANAN</a>
      <ul>
        <li><a href="?cat=administrator&page=viewadmin">PEMESANAN</a></li>
      </ul>
  	<?php
	}
	?>
  </ul>
</div>
<!--leftmenu-->

</div>
<!--mainleft--> 
<!-- END OF LEFT PANEL -->