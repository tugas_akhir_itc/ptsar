<CENTER><h3> MACAM SPLIT </H3></center>
<div class="row-fluid">
<ul class="thumbnails">
<center> 
<li class="span4">
<div class="thumbnail">       
                  <div class="caption">
                    <h3>Suplit 2/3</h3>
					<img src="img/suplit/suplit23.jpg">
                    <p>Batu Split Ukuran 20 - 30 mm (mili meter). Material batu split jenis ini banyak digunakan untuk bahan pengecoran lantai dan pengecoran atau pembetonan horizontal yang lain.</</p>
                  </div>
                </div>
</center>
</li>
<center>
<li class="span4">
<div class="thumbnail">
                  <div class="caption">
                    <h3>Suplit 2/1</h3>
					<img src="img/suplit/suplit21.jpg">
                    <p>Batu Split Ukuran 20 - 10 mm (mili meter). Material batu split jenis ini banyak digunakan untuk bahan  pengecoran segala macam konstruksi, mulai dari konstuksi ringan sampai konstruksi berat. Bangunan-bangunan yang menggunakan beton cor dari bahan batu split ukuran ini antara lain Jalan Tol, Gedung bertingkat, Landasan Pesawat Udara, Bantalan Kereta Api, Pelabuhan</p>
                  </div>
                </div>
</center>
</li>
<center>
<li class="span4">
<div class="thumbnail">
                  <div class="caption">
                    <h3>Suplit 5/1</h3>
					<img src="img/suplit/suplit05.jpg">
                    <p>Batu Split Ukuran 5 - 10 mm (mili meter) atau disebut juga dengan batu split ukuran 3/8 cm (centi meter). Material batu split jenis ini banyak digunakan untuk campuran dalam proses pengaspalan jalan, mulai dari jalan yang ringan sampai jalan kelas 1. Batu split jenis ukuran ini akan dicampur dengan aspal menjadi Aspal Mixed Plant atau secara umum disebut dengan aspal hot mixed.</p>
                  </div>
                </div>
</center>
</li>
<center>
<li class="span4">
<div class="thumbnail">
                  <div class="caption">
                    <h3>Abu Batu</h3>
					<img src="img/suplit/abubatu.jpg">
                    <p>Batu Split Ukuran 0 - 5 mm (mili meter). Jenis ini sering disebut juga dengan istilah Abu Batu. Ukuran ini merupakan jenis ukuran yang paling lembut, ukuran partikelnya menyerupai pasir lembut. Batu split jenis ukuran ini banyak dibutuhkan untuk campuran dalam proses pengaspalan atau dapat digunakan sebagai pengganti pasir. Material batu split ukuran ini merupakan bahan utama untuk pembuatan gorong-gorong dan batako press.</p>
                  </div>
                </div>
</center>
</li>
<center>
<li class="span4">
<div class="thumbnail">
                  <div class="caption">
                    <h3>LPA</h3>
					<img src="img/suplit/lpa.jpg">
                    <p>Batu Split Jenis Agregat A. Matreal batu split ini termasuk dalam jenis sirtu. Batu split jenis Agregat A ini merupakan campuran antara beberapa jenis ukuran baru split. Bahan campurannya terdiri dari abu batu, pasir, batu split ukuran 20 - 10 mm, batu split ukuran 20 - 30 mm, batu split ukuran 5 - 10 mm dan batu split 0 - 5 /abu batu. Pencampuran bahan ini tidak ada pedoman komposisi yang pasti atau baku dari masing-masing bahan. Komposisi disesuaikan dengan jenis penggunaannya. Batu split jenis Agregat A ini pada umumnya digunakan sebagai bahan pengecoran dinding, pembuatan dinding dan campuran bahan beton cor.</p>
                  </div>
                </div>
</center>
<center>
<li class="span4">
<div class="thumbnail">

                  <div class="caption">
                    <h3>LPB</h3>
					<img src="img/suplit/lpb.jpg">
                    <p>Batu Split Jenis Agregat B. Matreal batu split ini termasuk dalam jenis sirtu. Batu split jenis Agregat B ini merupakan campuran antara beberapa jenis ukuran baru split. Bahan campurannya terdiri dari tanah, abu batu, pasir, batu split ukuran 10-20 mm, batu split ukuran 20-30 mm dan batu split ukuran 30-50 mm. Bahan Tanah merupakan pembeda komposisi dengan batu split jenis Agregat A. Pencampuran bahan ini tidak ada pedoman komposisi yang pasti atau baku dari masing-masing bahan. Komposisi disesuaikan dengan jenis penggunaannya. Batu split jenis Agregat B ini pada umumnya digunakan untuk bahan timbunan awal pengerasan jalan dengan tujuan untuk meratakan dan mengikat lapisan batu split yang digelar pada lapisan di atasnya. </p>
                  </div>
                </div>
</center>
</li>
</ul>
</div>
