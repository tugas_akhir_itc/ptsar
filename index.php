<?php
include("_db.php");
?>
<!DOCTYPE html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>PT SAR</title>
	<?php include("_scr.php"); ?>
</head>

<body>

<div class="mainwrapper fullwrapper">
	
    <!-- START OF LEFT PANEL -->
    <div class="leftpanel">
    	
        <div class="logopanel">
        	<h1><a href="dashboard.php"></a></h1>
        </div><!--logopanel-->
        
        <div class="datewidget">Hari ini: <?php echo date("d M Y"); ?></div>
    	
        <div class="leftmenu">
  <ul class="nav nav-tabs nav-stacked">
    <li class=""><a href="dashboard.php"><span class="icon-align-justify"></span> DASHBOARD</a></li>
    
    <!--MENU administrator-->
	    <li class="dropdown"><a href="#"><span class="icon-pencil"></span> PROFIL PERUSAHAAN</a>
      <ul>
        <li><a href="?page=profil">PT SUMBER ARTHA RAYA</a></li>       
      </ul>
    </li>

    <li class="dropdown"><a href="#"><span class="icon-pencil"></span> INFORMASI STOK</a>
      <ul>
		<li><a href="?page=catalog">CATALOG</a></li>	
		<li><a href="?page=guest_view">STOK BARANG</a></li>	
      </ul>
    </li>
	<li class="dropdown"><a href="#"><span class="icon-pencil"></span> PEMESANAN</a>
	<ul>
        <li><a href="?page=pemesanan">PESAN</a></li>       
      </ul>
    </li>
</div>
<!--leftmenu-->

</div> <!--NAVIGASI MENU UTAMA-->
    
    <!-- START OF RIGHT PANEL -->
    <div class="rightpanel">
    	<div class="headerpanel">
        	<a href="" class="showmenu"></a>
            <div class="headerright">

            </div><!--headerright-->
    	</div><!--headerpanel-->
        
        <div class="breadcrumbwidget">
        	<ul class="breadcrumb">
                <li></li>
            </ul>
        </div> 
        <!--breadcrumbwidget-->
        
		<div class="pagetitle">
        	<h1><b>Selamat Datang Pengunjung</b><a href="dashboard.php"></a></h1> 
       	  <!--<span>This is a sample description for dashboard page...</span>-->
        </div><!--pagetitle-->
        
      <div class="maincontent">
       	<div class="contentinner content-dashboard">
            	<!--<div class="alert alert-info">
                	<button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>Welcome!</strong> This alert needs your attention, but it's not super important.
                </div>--><!--alert-->
                 <div class="row-fluid"><!--span8-->
                  <?php
				$v_cat = (isset($_REQUEST['cat'])&& $_REQUEST['cat'] !=NULL)?$_REQUEST['cat']:'';
				$v_page = (isset($_REQUEST['page'])&& $_REQUEST['page'] !=NULL)?$_REQUEST['page']:'';
				if(file_exists("pages/".$v_cat."/".$v_page.".php"))
				{
					include("pages/".$v_cat."/".$v_page.".php");
				}else{
					include("pages/catalog.php");
				}
				
				
				?>
                
                <!--span4-->
              </div>
                <!--row-fluid-->
          </div><!--contentinner-->
        </div><!--maincontent-->
        
    </div><!--mainright-->
    <!-- END OF RIGHT PANEL -->
    
    <div class="clearfix"></div>
    
	<!--FOOTER-->
    <?php include("_footer.php"); ?>
    
</div><!--mainwrapper-->
	<!--SLIDE NAVIGASI-->
    <?php include("_nav-slider.php"); ?>
</body>
</html>
