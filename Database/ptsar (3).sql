-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 02, 2017 at 03:28 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ptsar`
--

-- --------------------------------------------------------

--
-- Table structure for table `bahan`
--

CREATE TABLE IF NOT EXISTS `bahan` (
  `kode_bahan` varchar(255) NOT NULL,
  `nama_bahan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bahan`
--

INSERT INTO `bahan` (`kode_bahan`, `nama_bahan`) VALUES
('s21', 'suplit2/1');

-- --------------------------------------------------------

--
-- Table structure for table `bahan_digunakan`
--

CREATE TABLE IF NOT EXISTS `bahan_digunakan` (
  `id_bahan_digunakan` int(100) NOT NULL,
  `tgl` date NOT NULL,
  `kode_bahan` varchar(100) NOT NULL,
  `jumlah` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bahan_persediaan`
--

CREATE TABLE IF NOT EXISTS `bahan_persediaan` (
  `kode_bahan` varchar(100) NOT NULL,
  `stok_awal` varchar(100) NOT NULL,
  `masuk` varchar(100) NOT NULL,
  `keluar` varchar(100) NOT NULL,
  `stok_tersedia` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `barang_jadi`
--

CREATE TABLE IF NOT EXISTS `barang_jadi` (
  `kode_barang` varchar(20) NOT NULL,
  `nama_barang` varchar(40) NOT NULL,
  `jenis_barang` varchar(10) NOT NULL,
  `harga` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang_jadi`
--

INSERT INTO `barang_jadi` (`kode_barang`, `nama_barang`, `jenis_barang`, `harga`) VALUES
('S21', 'Suplit 2/1', 'Batu Pecah', 200000);

-- --------------------------------------------------------

--
-- Table structure for table `data_persediaan`
--

CREATE TABLE IF NOT EXISTS `data_persediaan` (
  `kode_barang` varchar(40) NOT NULL,
  `stok_awal` varchar(10) NOT NULL DEFAULT '0',
  `masuk` varchar(10) NOT NULL DEFAULT '0',
  `keluar` varchar(10) NOT NULL DEFAULT '0',
  `stok_tersedia` varchar(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `entry_bahan`
--

CREATE TABLE IF NOT EXISTS `entry_bahan` (
  `id_entry_bahan` int(100) NOT NULL,
  `tgl` date NOT NULL,
  `kode_bahan` varchar(100) NOT NULL,
  `jumlah` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `entry_barang_jadi`
--

CREATE TABLE IF NOT EXISTS `entry_barang_jadi` (
  `id_entry_barang_jadi` int(20) NOT NULL,
  `tgl` date NOT NULL,
  `kode_barang` varchar(30) NOT NULL,
  `jumlah` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pemesanan`
--

CREATE TABLE IF NOT EXISTS `pemesanan` (
  `id_pemesanan` int(100) NOT NULL,
  `tgl` date NOT NULL,
  `kode_barang` varchar(100) NOT NULL,
  `jumlah` int(100) NOT NULL,
  `nama_pemesan` varchar(100) NOT NULL,
  `kontak_pemesan` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_login`
--

CREATE TABLE IF NOT EXISTS `user_login` (
  `username` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL,
  `login_hash` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_login`
--

INSERT INTO `user_login` (`username`, `password`, `login_hash`) VALUES
('admin', '81dc9bdb52d04dc20036dbd8313ed055', 'administrator');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bahan`
--
ALTER TABLE `bahan`
  ADD PRIMARY KEY (`kode_bahan`);

--
-- Indexes for table `bahan_digunakan`
--
ALTER TABLE `bahan_digunakan`
  ADD PRIMARY KEY (`id_bahan_digunakan`);

--
-- Indexes for table `bahan_persediaan`
--
ALTER TABLE `bahan_persediaan`
  ADD PRIMARY KEY (`kode_bahan`);

--
-- Indexes for table `barang_jadi`
--
ALTER TABLE `barang_jadi`
  ADD PRIMARY KEY (`kode_barang`);

--
-- Indexes for table `data_persediaan`
--
ALTER TABLE `data_persediaan`
  ADD PRIMARY KEY (`kode_barang`);

--
-- Indexes for table `entry_bahan`
--
ALTER TABLE `entry_bahan`
  ADD PRIMARY KEY (`id_entry_bahan`);

--
-- Indexes for table `entry_barang_jadi`
--
ALTER TABLE `entry_barang_jadi`
  ADD PRIMARY KEY (`id_entry_barang_jadi`);

--
-- Indexes for table `pemesanan`
--
ALTER TABLE `pemesanan`
  ADD PRIMARY KEY (`id_pemesanan`);

--
-- Indexes for table `user_login`
--
ALTER TABLE `user_login`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bahan_digunakan`
--
ALTER TABLE `bahan_digunakan`
  MODIFY `id_bahan_digunakan` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `entry_bahan`
--
ALTER TABLE `entry_bahan`
  MODIFY `id_entry_bahan` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `entry_barang_jadi`
--
ALTER TABLE `entry_barang_jadi`
  MODIFY `id_entry_barang_jadi` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `pemesanan`
--
ALTER TABLE `pemesanan`
  MODIFY `id_pemesanan` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
